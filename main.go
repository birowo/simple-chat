package main

import (
	"bytes"
	"fmt"
	"io"
	"net"
	"net/http"
	"sync"

	"github.com/gobwas/ws"
)

type Room map[net.Conn]struct{}

var (
	rooms   = map[string]Room{}
	inRooms = map[net.Conn][]string{}
	mu      sync.Mutex
)

func wsServer(w http.ResponseWriter, r *http.Request) {
	conn, _, _, err := ws.UpgradeHTTP(r, w)
	if err != nil {
		// handle error
		fmt.Println("0", err) //*
		return
	}
	var roomName string
	go func() {
		defer func() {
			mu.Lock()
			for _, roomName := range inRooms[conn] {
				delete(rooms[roomName], conn)
				if len(rooms[roomName]) == 0 {
					delete(rooms, roomName)
				}
			}
			//fmt.Println(rooms)
			delete(inRooms, conn)
			mu.Unlock()
			fmt.Println("close")
			conn.Close()
		}()
		for {
			header, err := ws.ReadHeader(conn)
			if err != nil {
				// handle error
				fmt.Println("1", err, header) //*
				return
			}
			if header.OpCode == ws.OpClose {
				fmt.Println("OpClose")
				return
			}
			payload := make([]byte, header.Length)
			_, err = io.ReadFull(conn, payload)
			if err != nil {
				// handle error
				fmt.Println("2", err, payload) //*
			}
			if header.Masked {
				ws.Cipher(payload, header.Mask, 0)
			}
			// Reset the Masked flag, server frames must not be masked as
			// RFC6455 says.
			header.Masked = false
			if header.OpCode == ws.OpPing { //if there is PING request from client
				header.OpCode = ws.OpPong //response with PONG
			}
			pos := int64(bytes.Index(payload, []byte(`","`)))
			if header.Length > 8 && pos > 2 && (pos+3) < (header.Length-2) {
				roomName = string(payload[2:pos])
				payload = payload[pos+3 : header.Length-2]
				header.Length = int64(len(payload))
				mu.Lock()
				_, ok := rooms[roomName]
				if ok {
					rooms[roomName][conn] = struct{}{}
				} else {
					rooms[roomName] = Room{conn: struct{}{}}
				}
				inRooms[conn] = append(inRooms[conn], roomName)
				mu.Unlock()
			}
			mu.Lock()
			for conn, _ := range rooms[roomName] {
				mu.Unlock()
				//fmt.Println(conn, header, string(payload))
				if err := ws.WriteHeader(conn, header); err != nil {
					// handle error
					fmt.Println("3", err, header) //*
				}
				if _, err := conn.Write(payload); err != nil {
					// handle error
					fmt.Println("4", err, payload) //*
				}
				mu.Lock()
			}
			mu.Unlock()
		}
	}()
}
func main() {
	http.Handle("/", http.FileServer(http.Dir("./client")))
	http.HandleFunc("/ws", wsServer)
	http.ListenAndServe(":8080", nil)
}

//*: Println err for error tracing
