# simple-chat

**simple-chat using gobwas library**

**gobwas: [https://github.com/gobwas/ws](https://github.com/gobwas/ws)**

**client-code(index.html) taken from: [https://javascript.info/websocket](https://javascript.info/websocket)**

**install: `go get -u gitlab.com/birowo/simple-chat`**

**general reference: [https://developer.mozilla.org/en-US/docs/Web/API/WebSockets_API/Writing_WebSocket_servers](https://developer.mozilla.org/en-US/docs/Web/API/WebSockets_API/Writing_WebSocket_servers)**